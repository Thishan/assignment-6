#include <stdio.h>

int fibonacciSeq(int n) {
	
	if(n == 0) return 0;
	
	if(n == 1) return 1;
	
	return fibonacciSeq(n-1) + fibonacciSeq(n-2);
	 
}

int main() {
	
	int n = 0; 
	int l;
	printf("Enter Seq :");
	scanf("%d",&l);
	
	for(n =0;n<=l;n++){
		printf("%d\n",fibonacciSeq(n));
	}
	return 0;
}

